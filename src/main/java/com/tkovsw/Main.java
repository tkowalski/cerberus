package com.tkovsw;
import java.io.File;
import java.io.IOException;

import org.apache.commons.scxml.model.ModelException;
import org.xml.sax.SAXException;


public class Main {
	public static void main(String[] args) throws IOException, 
		SAXException, ModelException, InterruptedException {
		if(isFileInArgs(args)){
			new Cerberus(args[0]).start();
		}else{
			System.exit(1);
		}
	}

	private static boolean isFileInArgs(String[] args) {
		if(args.length == 0){
			return false;
		}
		File f = new File(args[0]);
		return f.exists() && f.isFile();
	}
}
