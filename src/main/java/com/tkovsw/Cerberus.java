package com.tkovsw;
import java.io.IOException;

import org.apache.commons.scxml.Context;
import org.apache.commons.scxml.SCXMLExecutor;
import org.apache.commons.scxml.TriggerEvent;
import org.apache.commons.scxml.env.SimpleErrorHandler;
import org.apache.commons.scxml.env.SimpleErrorReporter;
import org.apache.commons.scxml.env.jexl.JexlContext;
import org.apache.commons.scxml.env.jexl.JexlEvaluator;
import org.apache.commons.scxml.io.SCXMLParser;
import org.apache.commons.scxml.model.ModelException;
import org.apache.commons.scxml.model.SCXML;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


public class Cerberus {
	private String scxmlUrl;
	private SCXML scxml;
	private SCXMLExecutor executor;
	
	public Cerberus(String scxmlUrl){
		this.scxmlUrl = scxmlUrl;
	}
	
	public void start() throws IOException, SAXException, ModelException, InterruptedException{
		prepareSCXML();
		
		executor.go();
		
		sendEvents();
		
		while(!executor.getCurrentStatus().isFinal()){
			Thread.sleep(250);
		}
	}
	
	private void prepareSCXML() throws IOException, SAXException,
			ModelException {
		
		scxml = SCXMLParser.parse(new InputSource(scxmlUrl), new SimpleErrorHandler()); //(1)
		
		executor = new SCXMLExecutor(new JexlEvaluator(), //(2) 
				null,
				new SimpleErrorReporter());
		executor.setEventdispatcher(new CerberusEventDispatcher(executor)); //(3)
		
		Context context = new JexlContext(); //(4)
		executor.setRootContext(context); //(5)
		executor.setStateMachine(scxml); //(6)
	}
	
	private void sendEvents() throws ModelException, InterruptedException{
		sendEventAndWait(new TriggerEvent("arm", TriggerEvent.SIGNAL_EVENT), 6000);
		sendEventAndWait(new TriggerEvent("door_opened", TriggerEvent.SIGNAL_EVENT), 1000);
		sendEventAndWait(new TriggerEvent("window_opened", TriggerEvent.SIGNAL_EVENT), 1000);
		sendEventAndWait(new TriggerEvent("disarm", TriggerEvent.SIGNAL_EVENT), 1000);
		sendEventAndWait(new TriggerEvent("finish", TriggerEvent.SIGNAL_EVENT), 1000);
	}
	
	public void sendEventAndWait(TriggerEvent event, long sleepMs) throws ModelException, InterruptedException{
		executor.triggerEvent(event);
		Thread.sleep(sleepMs);
	}
}
