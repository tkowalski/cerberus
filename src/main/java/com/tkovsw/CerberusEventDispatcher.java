package com.tkovsw;

import java.util.List;
import java.util.Map;

import org.apache.commons.scxml.SCXMLExecutor;
import org.apache.commons.scxml.env.SimpleScheduler;

public class CerberusEventDispatcher extends SimpleScheduler{
	private static final long serialVersionUID = 1L;
	
	public static final String CERB_TYPE = "CERB_TYPE";

	public CerberusEventDispatcher(SCXMLExecutor executor) {
		super(executor);
	}

	public void cancel(String sendId) {
		super.cancel(sendId);
	}

	@Override
	public void send(String sendId, String target, String targettype,
			String event, Map params, Object hints, long delay,
			List externalNodes) {
		System.out.println("send: sendId=" + sendId + " target=target" + " targetType=" + targettype +
				" event=" + event + " params=" + params + " hints=" + hints + " delay=" + delay
				+ " externalNodes=" + externalNodes);
		
		if(CERB_TYPE.equalsIgnoreCase(targettype)){
			if("turn_on_alarm".equals(event)){
				System.out.println("ALARM!!!");
			}else if("turn_off_alarm".equals(event)){
				System.out.println("silence");
			}
		}else{
			super.send(sendId, target, targettype, event, params, hints, delay, externalNodes);
		}
		
		
	}
}